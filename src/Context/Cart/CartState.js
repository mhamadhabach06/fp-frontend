import { useReducer } from "react";
import CartContext from "./CartContext";
import CartReducer from "./CartReducer";
import { SHOW_HIDE_CART, ADD_TO_CART, REMOVE_ITEM,Remove_all_items } from "../Types";

let cart = JSON.parse(localStorage.getItem('cart'))
if (cart) cart.showCart = false 
const CartState = ({ children }) => {
  const initalState = cart ? cart : {
    showCart: false,
    cartItems: [],
  };

  const [state, dispatch] = useReducer(CartReducer, initalState);

  const addToCart = (item) => {
    dispatch({ type: ADD_TO_CART, payload: item });
  };

  const showHideCart = () => {
    dispatch({ type: SHOW_HIDE_CART });
  };

  const removeItem = (id) => {
    dispatch({ type: REMOVE_ITEM, payload: id });
  };

  const removeAllItem = () => {
    dispatch({ type: Remove_all_items });
  };

  return (
    <CartContext.Provider
      value={{
        showCart: state.showCart,
        cartItems: state.cartItems,
        addToCart,
        showHideCart,
        removeItem,
        removeAllItem
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

export default CartState;