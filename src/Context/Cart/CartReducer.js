import { SHOW_HIDE_CART, ADD_TO_CART, REMOVE_ITEM,Remove_all_items } from "../Types";

const CartReducer = (state, action) => {
  switch (action.type) {
    case SHOW_HIDE_CART: {
      return {
        ...state,
        showCart: !state.showCart,
      };
    }
    case ADD_TO_CART: {
      const checkIfExist = state.cartItems.findIndex(
        (ele) => ele.item.id === action.payload.item.id
      );
      if (checkIfExist !== -1) {
        state.cartItems[checkIfExist].quantity = action.payload.quantity;
        localStorage.setItem("cart", JSON.stringify(state));
        return state;
      }
      localStorage.setItem("cart", JSON.stringify({
        ...state,
        cartItems: [
          ...state.cartItems,
          { item: action.payload.item, quantity: action.payload.quantity },
        ],
      }));
      return {
        ...state,
        cartItems: [
          ...state.cartItems,
          { item: action.payload.item, quantity: action.payload.quantity },
        ],
      };
    }
    case REMOVE_ITEM: {
      localStorage.setItem("cart", JSON.stringify({
        ...state,
        cartItems: state.cartItems.filter(
          (item) => item.item.id !== action.payload
        ),
      }));
      return {
        ...state,
        cartItems: state.cartItems.filter(
          (item) => item.item.id !== action.payload
        ),
      };
    }

    case Remove_all_items:{
      localStorage.setItem("cart",JSON.stringify({
        showCart: false,
        cartItems: [],
      }))
      return {
        showCart: false,
        cartItems: [],
      }
    }

    default:
      return state;
  }
};

export default CartReducer;
