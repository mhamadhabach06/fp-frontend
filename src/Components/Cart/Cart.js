import React, { useContext, useEffect } from 'react'
import "./Cart.css"
import CartContext from '../../Context/Cart/CartContext'
import CartItem from './CartItem';
import { useNavigate } from "react-router-dom";


const Cart = () => {
    const { showCart, cartItems, showHideCart } = useContext(CartContext);
    const navigate = useNavigate();
    let token = localStorage.getItem("token");

    const handleCheckout = () =>{
      if(!token){
        navigate("/signup")
      }else{
        navigate("checkout")
      }
    }


  return (
    <>
      {showCart && (
        <div className='cart__wrapper'>
          <div style={{ textAlign: "right" }}>
            <i
              style={{ cursor: "pointer" }}
              className='fa fa-times-circle'
              aria-hidden='true'
              onClick={showHideCart}
            ></i>
          </div>
          <div className='cart__innerWrapper'>
            {cartItems.length === 0 ? (
              <h4>Cart is Empty</h4>
            ) : (
              <ul>
                {cartItems.map((item) => (
                  <CartItem key={item._id} item={item} />
                ))}
              </ul>
              
            )}
          </div>
          <div className='Cart__cartTotal'>
            <div>Cart Total</div>
            <div></div>
            <div style={{ marginLeft: 5 }}>
              {
                cartItems.reduce((amount, item) => item.item.price * item.quantity  + amount, 0) 
              }
            </div>
          </div>
          {cartItems.length !== 0 ? (<button onClick={handleCheckout}  className='checkout'>Proceed To Checkout</button>):""}

        </div>
      )}
    </>
  )
}

export default Cart