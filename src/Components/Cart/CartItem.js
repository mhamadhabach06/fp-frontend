import React from 'react'
import { useContext } from "react";
import "./CartItem.css";
import CartContext from '../../Context/Cart/CartContext';
import DeleteIcon from "@mui/icons-material/Delete";

const CartItem = ({item}) => {
    // console.log('item',item);
    const { removeItem } = useContext(CartContext);
    return (
      <li className='CartItem__item'>
        <img src={item.item.img} alt='' />
        <div>
          {item.item.name}
        </div>
        <button style={{background:"none"}} className='CartItem__button' onClick={() => removeItem(item.item.id)}>
          <DeleteIcon sx={{color:"red"}}/>
        </button>
      </li>
    );
}

export default CartItem