import React, { useContext, useEffect, useState } from "react";
import "./NavBar.css";
import { Link } from "react-router-dom";
import logo from "../../imgs/logo.png";
import { IconButton } from "@mui/material";
import { styled } from "@mui/material/styles";
import Badge from "@mui/material/Badge";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import CartContext from "../../Context/Cart/CartContext";
import { useNavigate } from "react-router-dom";
import Cart from "../Cart/Cart";
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import axios from "axios";

const StyledBadge = styled(Badge)(({ theme }) => ({
  "& .MuiBadge-badge": {
    right: -3,
    top: 13,
    border: `2px solid ${theme.palette.background.paper}`,
    padding: "0 4px",
  },
}));

const NavBar = () => {
  const navigate = useNavigate();

  const { cartItems, showHideCart } = useContext(CartContext);

  const [toggle, setToggle] = useState(false);
  const [points,setPoints] = useState(false);
  let token = localStorage.getItem("token");
  const urlPoints = process.env.REACT_APP_URL+"/users/me";

  const toggleHandler = () => {
    setToggle(!toggle);
  };

  const getPoints = async () => {
    try{
      const response = await axios.get(`${urlPoints}`,{
        headers:{
          Authorization:`Bearer ${token}`
        }
      });
      setPoints(response.data); 
      console.log(response.data)
    }catch(err){
      console.log(err)
    }
  }

  useEffect(() => {
    getPoints()
  },[points])

  return (
    <nav>
      <div className="logo">
        <img src={logo} alt="logo" />
        <Link to="/">
          <h1>Plantify</h1>
        </Link>
      </div>

      <div className="home-ul2">
        <ul className={toggle ? "active" : ""}>
          <li>
            <Link onClick={() => setToggle(false)} to="/">
              Home
            </Link>
          </li>
          <li>
            <Link onClick={() => setToggle(false)} to="/products">
              Shop
            </Link>
          </li>

          <li>
            <Link onClick={() => setToggle(false)} to="/contact">
              Contact Us
            </Link>
          </li>

          {token ? (
            <>
            <li>
              <Link
                onClick={() => {
                  setToggle(false);
                  localStorage.removeItem("token");
                  navigate("/");
                }}
                to="/"
              >
                Logout
              </Link>
            </li>

              <li>
                <Badge badgeContent={points.points} color="success">
                   <RadioButtonUncheckedIcon/>
                </Badge>
              </li>
              </>
          ) : (
            <Link
              className="signup-btn"
              onClick={() => setToggle(false)}
              to="/signup"
            >
              Sign Up
            </Link>
          )}
          <li className="cart-icon">
            <IconButton aria-label="cart">
              <StyledBadge
                badgeContent={cartItems.length}
                color="secondary"
                onClick={showHideCart}
              >
                <ShoppingCartIcon />
              </StyledBadge>
            </IconButton>
          </li>
        </ul>
      </div>

      <div onClick={toggleHandler} className="home-toggle-btn">
        <i className={toggle ? "fa-solid fa-xmark" : "fa-solid fa-bars"}></i>
      </div>
    </nav>
  );
};

export default NavBar;
