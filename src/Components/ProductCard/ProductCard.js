import React, { useContext } from "react";
import { Link } from "react-router-dom";
import "./ProductCard.css";
import CartContext from "../../Context/Cart/CartContext";

const ProductCard = ({ id, img, name, price, points, desc }) => {
  const { addToCart } = useContext(CartContext);

  return (
    <>
      <Link className="s" to="/productDetail" state={{ id: id }}>
        <div className="card" /*onClick={handleNavigate}*/>
          <img src={img} alt="tree-img" />
          <h1>{name}</h1>
          <p className="price">
            Price: {price}$ - Points: {points}
          </p>
          <p>{desc}</p>
          <Link
            // to="/productDetail"
            // state={{ id: id }}
            onClick={() =>
              addToCart({
                item: { id, img, name, price, points, desc },
                quantity: 1,
              })
            }
          >
            Add to Cart{" "}
          </Link>
        </div>
      </Link>
    </>
  );
};

export default ProductCard;
