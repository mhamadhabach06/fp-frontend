import React, { useState, useEffect } from "react";
import "./Footer.css";
import { Link } from "react-router-dom";
import axios from "axios";
function Footer() {
  
  return (
    <footer className="footer">
      <div className="footer-container">
        <div className="footer-row">
          <div className="footer-col">
            <h4>Plantify</h4>
            <p>
            We're on a mission<br/> to plant trees, green communities and empower environmental stewards across Lebanon.
            </p>
          </div>
          <div className="footer-col">
            <h4>Reach us</h4>
            <ul>
              <li>
                <Link to="/contact">Contact</Link>
              </li>
            </ul>
          </div>
           <div className="footer-col">
           <h4>Get Help</h4>
            <ul>
              <li>FAQ</li>
              <li>Shipping</li>
              <li>Track Your Order</li>
            </ul>
          </div> 
          <div className="footer-col">
            <h4>follow-us</h4>
            <div className="social-links">
              <a href="https://www.instagram.com/">
                <i className="fa-brands fa-instagram"></i>
              </a>
              <a href="https://www.facebook.com/">
                <i className="fa-brands fa-facebook-f"></i>
              </a>
              <a href="https://www.linkedin.com/">
                <i className="fa-brands fa-linkedin-in"></i>
              </a>
              <a href="https://twitter.com/">
                <i className="fa-brands fa-twitter"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
