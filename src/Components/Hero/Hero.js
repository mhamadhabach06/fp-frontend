import React from "react";
import "./Hero.css";
import heroimg from "../../imgs/hero-img.svg";
import { Link } from "react-router-dom";
const Hero = () => {
  return (
    <div className="hero-container">
      <div className="hero-left">
        <h2>We're on a mission to plant trees, green communities.</h2>
        <p>Empowering environmental stewards across Lebanon.</p>
        <Link to="/products" className="hero-shopnow">Shop Now</Link>
      </div>

      <div className="hero-img">
        <img src={heroimg} alt="hero img" />
      </div>
    </div>
  );
};

export default Hero;
