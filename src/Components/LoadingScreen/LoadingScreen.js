import React from "react";
import './LoadingScreen.css'



const LoadingScreen = () => {

    return (
        <>
            <div class="screen">

                <div class="loader">
                    <div class="dot"></div>
                    <div class="dot"></div>
                    <div class="dot"></div>
                </div>

            </div>
        </>
    )
}

export default LoadingScreen