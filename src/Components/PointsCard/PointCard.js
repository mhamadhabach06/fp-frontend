import React, { useEffect } from "react";
import "./PointCard.css";
import { useNavigate } from "react-router-dom";
import Aos from "aos";
import "aos/dist/aos.css";
const PointCard = () => {
  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);

  const navigate = useNavigate();
  const navigateToShop = () => {
    navigate("/products");
  };

  return (
    <>

      <div className="pointcardcont">
      <h2 className="title-points">Our Point System</h2>

        <div className="wrapper">
          <div className="table basic" data-aos="slide-right">
            <div className="head_tab">
              <h2>
                Single
                <br /> Package
              </h2>
            </div>
            <div className="aj_p">
              <p style={{ paddingTop: "0.7rem" }}>
                Ideal solution for beginners
              </p>
            </div>
            <div className="price-section">
              <div className="price-area">
                <div className="inner-area">
                  <span className="text"></span>
                  <span className="price">
                    200<span style={{ fontSize: "12px" }}>/mo</span>
                  </span>
                </div>
              </div>
            </div>
            <div className="package-name"></div>
            <ul className="features">
              <div className="btn">
                <button onClick={navigateToShop}>Shop Now</button>
              </div>
              <p className="aj_des">You will get</p>
              <li>
                <span className="list-name">1 Free Tree.</span>
                <span className="icon check">
                  <i className="fas fa-check"></i>
                </span>
              </li>
              <li>
                <span className="list-name">15% Discount.</span>
                <span className="icon check">
                  <i className="fas fa-check"></i>
                </span>
              </li>
              <li>
                <span className="list-name">Tree & Shrub Fertilization.</span>
                <span className="icon cross">
                  <i className="fas fa-times"></i>
                </span>
              </li>
            </ul>
          </div>
          <div className="table premium" data-aos="slide-up">
            <div className="head_tab">
              <h2>
                Premium <br />
                Package
              </h2>
            </div>
            <div className="aj_p">
              <p style={{ paddingTop: "0.7rem" }}>
                Perfect package for Environmental specialist
              </p>
            </div>
            <div className="ribbon">
              <span>BEST VALUE</span>
            </div>
            <div className="price-section">
              <div className="price-area">
                <div className="inner-area">
                  <span className="text"></span>
                  <span className="price">
                    400<span style={{ fontSize: "12px" }}>/mo</span>
                  </span>
                </div>
              </div>
            </div>
            <div className="package-name"></div>
            <ul className="features">
              <div className="btn">
                <button onClick={navigateToShop}>Shop Now</button>
              </div>
              <p className="aj_des">You will get</p>
              <li>
                <span className="list-name">1 Free Tree.</span>
                <span className="icon check">
                  <i className="fas fa-check"></i>
                </span>
              </li>
              <li>
                <span className="list-name">Wood Inspection Systems.</span>
                <span className="icon check">
                  <i className="fas fa-check"></i>
                </span>
              </li>
              <li>
                <span className="list-name">Tree & Shrub Fertilization.</span>
                <span className="icon check">
                  <i className="fas fa-check"></i>
                </span>
              </li>

              <li>
                <span className="list-name">Shrub Pruning.</span>
                <span className="icon check">
                  <i className="fas fa-check"></i>
                </span>
              </li>
            </ul>
          </div>
          <div className="table ultimate" data-aos="slide-left">
            <div className="head_tab">
              <h2>
                Ultimate <br />
                Package
              </h2>
            </div>
            <div className="aj_p">
              <p style={{ paddingTop: "0.7rem" }}>
                Perfect package for businesses
              </p>
            </div>
            <div className="price-section">
              <div className="price-area">
                <div className="inner-area">
                  <span className="text"></span>
                  <span className="price">
                    300<span style={{ fontSize: "12px" }}>/mo</span>
                  </span>
                </div>
              </div>
            </div>
            <div className="package-name"></div>
            <ul className="features">
              <div className="btn">
                <button onClick={navigateToShop}>Shop Now</button>
              </div>
              <p className="aj_des">You will get</p>
              <li>
                <span className="list-name">1 Free Tree.</span>
                <span className="icon check">
                  <i className="fas fa-check"></i>
                </span>
              </li>
              <li>
                <span className="list-name">30% Discount.</span>
                <span className="icon check">
                  <i className="fas fa-check"></i>
                </span>
              </li>
              <li>
                <span className="list-name">Tree & Shrub Fertilization.</span>
                <span className="icon cross">
                  <i className="fas fa-times"></i>
                </span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

export default PointCard;
