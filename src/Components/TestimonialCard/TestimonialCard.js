import React, { useEffect } from "react";
import "./TestimonialCard.css";
import prof1 from "../../imgs/profile-1.jpeg"
import prof2 from "../../imgs/profile-2.jpeg"
import prof3 from "../../imgs/profile-3.jpeg"
import Aos from "aos";
const TestimonialCard = () => {
  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);
  return (
    <>
    <div className="testi-container">
          <h2 className="testi-title">Testimonials</h2>

      <div class="wrapper-testi">
        <div class="box-testi" data-aos="slide-right">
          <i class="fas fa-quote-left quote"></i>
          <p>
          It has been a great pleasure working with you, and especially Joel who has that magic touch managing the plants that flourished in our suite. 
          </p>
          <div class="content-testi">
            <div class="info">
              <div class="name-testi">Joe Smith</div>
              <div class="job-testi">Business Man</div>
              <div class="stars">
              <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
              </div>
            </div>
            <div class="image-testi">
              <img src={prof1} alt="profile pic" />
            </div>
          </div>
        </div>

        <div class="box-testi" data-aos="slide-up">
          <i class="fas fa-quote-left quote"></i>
          <p>
          Everyone in our office appreciates your call and wants to thank you, Lisa, and the entire company of Plantify for all of your handwork.
          Lisa has been a fantastic representative of your company.
          </p>
          <div class="content-testi">
            <div class="info">
              <div class="name-testi">David Coulthard</div>
              <div class="job-testi">Enviromental Specialist</div>
              <div class="stars">
              <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
              </div>
            </div>
            <div class="image-testi">
              <img src={prof2} alt="profile pic" />
            </div>
          </div>
        </div>

        <div class="box-testi" data-aos="slide-left">
          <i class="fas fa-quote-left quote"></i>
          <p>
          Plantify always went the extra mile to keep our plants looking beautiful and when there was a need for a change out, she made sure we were taken care of quickly.
          </p>
          <div class="content-testi">
            <div class="info">
              <div class="name-testi">Alex Duran</div>
              <div class="job-testi">Agriculture Engineer</div>
              <div class="stars">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
              </div>
            </div>
            <div class="image-testi">
              <img src={prof3} alt="profile pic" />
            </div>
          </div>
        </div>
      </div>

      </div>
    </>
  );
};

export default TestimonialCard;
