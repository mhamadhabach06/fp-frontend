import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import LoadingScreen from "../../Components/LoadingScreen/LoadingScreen";
import "./ProductDetail.css";
import CartContext from "../../Context/Cart/CartContext";

const ProductDetail = () => {
  const {addToCart} = useContext(CartContext)

  const [oneProduct, setOneProduct] = useState([]);
  const [loading, setLoading] = useState(true);
  const [quantity, setQuantity] = useState(1);
  const [index,setIndex]=useState(0)

  const location = useLocation();
  const id = location.state.id;
  // console.log(id)

  //Fetch Specific Product
  const fetchProduct = async () => {
    try {
      const response = await axios.get(
        `${process.env.REACT_APP_URL}/products/${id}`
      );
      setOneProduct(response.data);
      setLoading(false);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    fetchProduct();
  }, []);

  const handlePlus = () => {
    setQuantity(quantity+1);
  };

  const handleMinus = () => {
    if (quantity > 1) {
      setQuantity(quantity-1);
    }
  };

  const addItemToCart = () => {
    addToCart({
      item:{id:oneProduct._id,img:oneProduct.file[0],name:oneProduct.name,price:oneProduct.price,points:oneProduct.points,desc:oneProduct.desc},
      quantity:quantity
    })
  }
  return (
    <>
      {loading ? (
        <LoadingScreen />
      ) : (
        <div className="prodDetail-container">
          <div className="prodDetail-left">
            <img src={oneProduct.file[index]} alt="image plant 1" />
            <div className="subimage-wrapper">
              <div className="subimage">
                {oneProduct.file.map((ele,i)=> (
                  <img key={i} src={ele} alt="image plant 1" onClick={()=>setIndex(i)}/>
                ))}
              </div>
            </div>
          </div>

          <div className="prodDetail-right">
            <h2>{oneProduct.name}</h2>
            <br />
            <p>{oneProduct.desc}</p><br />
            <p>Price: {oneProduct.price}$</p>

            <div className="wrapper-counter">
              <span onClick={handleMinus} className="minus">
                -
              </span>
              <span className="num">{quantity}</span>
              <span onClick={handlePlus} className="plus">
                +
              </span>
            </div>

            <div className="prodDeatil-addCart"  onClick={addItemToCart}>Add to Cart</div>
          </div>
        </div>
      )}
    </>
  );
};

export default ProductDetail;
