import { TextField } from "@mui/material";
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import CartContext from "../../Context/Cart/CartContext";
import { Remove_all_items } from "../../Context/Types";
import "./Checkout.css";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
const Checkout = () => {
  const [fullname, setFullName] = useState("");
  const [street, setStreet] = useState("");
  const [city, setCity] = useState("");
  const checkoutURL = process.env.REACT_APP_URL+"/orders"

  const notify = () => toast.success("Thank you.Your order has been placed");


  const { showCart, cartItems, showHideCart,removeAllItem } = useContext(CartContext);
  const navigate = useNavigate();
  useEffect(() => {
    if (!localStorage.getItem("token")) {
      navigate("/signup");
    }
  });

  const submitOrder = () => {
    placeOrder();
    // if (fullname && street && city) {
    //   setFullName("");
    //   setStreet("");
    //   setCity("");
    // }
  };

  const placeOrder = async () => {
    const orderItems=cartItems.map((e)=> ({
      "name":e.item.name,
      "qty":e.quantity,
      "price":e.item.price,
      "product":e.item.id,
      'points':e.item.points
    }))
    try {
      const res = await axios.post(`${checkoutURL}`, {
        "addressDetails":{
          "street":street,
          "country":"Lebanon",
          "city":city
        },
        "orderItems":orderItems
      }, {
        headers: {
          Authorization:`Bearer ${localStorage.getItem('token')}`
        }
      });
      console.log('res',res);
     if(res.status == 201){
      removeAllItem()
      notify()
      setFullName("");
      setStreet("");
      setCity("");
     }

    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="checkout-container">
      <div className="checkout-left">
        <h2>Billing Details</h2>
        <form className="form-checkout">
          <TextField
            value={fullname}
            onChange={(e) => setFullName(e.target.value)}
            required
            className="input-checkout"
            sx={{ width: "20rem" }}
            id="outlined-basic"
            label="Full Name"
            variant="outlined"
          />
          <TextField
            className="input-checkout"
            sx={{ width: "20rem" }}
            id="outlined-basic"
            value={"Lebanon"}
            disabled
            label="Country"
            variant="outlined"
          />
          <TextField
            value={city}
            onChange={(e) => setCity(e.target.value)}
            required
            className="input-checkout"
            sx={{ width: "20rem" }}
            id="outlined-basic"
            label="City"
            variant="outlined"
          />
          <TextField
            value={street}
            required
            onChange={(e) => setStreet(e.target.value)}
            className="input-checkout"
            sx={{ width: "20rem" }}
            id="outlined-basic"
            label="Street"
            variant="outlined"
          />
        </form>
      </div>

      <div className="checkout-right">
        <h2>Your Order</h2>
        <div className="yourorder-container">
          <div className="yourorder-title">
            <h3>Product</h3>
            <h3>Price</h3>
          </div>
          <div className="yourorder-details">
            {cartItems.map((item) => {
              console.log(cartItems);
              return (
                <>
                  <div className="details-map">
                    <p>
                      {item.item.name} &nbsp; ×{item.quantity}
                    </p>
                    <p>{item.item.price} $</p>
                  </div>
                </>
              );
            })}
            <button onClick={placeOrder} className="place-order">Place Order</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Checkout;
