import React from "react";
import Hero from "../../Components/Hero/Hero";
import NavBar from "../../Components/Navbar/NavBar";
import PointCard from "../../Components/PointsCard/PointCard";
import TestimonialCard from "../../Components/TestimonialCard/TestimonialCard";
import "./HomePage.css";
import Footer from "../../Components/Footer/Footer.js";
// import Footer from "../../Components/Footer/Footer";
const HomePage = () => {
  return (
    <>
      <div className="Home-container">
        <NavBar />  
        <Hero />
      </div>
      <div>
        <PointCard/>
      </div>
      <div>
        <TestimonialCard/>
      </div>
      <Footer/>

      
    </>
  );
};

export default HomePage;
