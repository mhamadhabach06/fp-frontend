import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import LoadingScreen from "../../Components/LoadingScreen/LoadingScreen";
import ProductCard from "../../Components/ProductCard/ProductCard";
import "./ProductsPage.css";

const ProductsPage = () => {
  const [categories, setCategories] = useState([]);
  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);

  const [isActive, setIsActive] = useState(-1);
  const [isActiveAll, setIsActiveAll] = useState(true);

  const [catType, setCatType] = useState();

  const [loading, setLoading] = useState(true);

  const url = process.env.REACT_APP_URL + "/category";
  const urlProduct = process.env.REACT_APP_URL + "/products";

  const handleAllProducts = () => {
    setFilteredProducts(products);
    setIsActiveAll(true);
    setIsActive(-1);
  };

  //Fetch Categories
  const getCategories = async () => {
    try {
      const response = await axios.get(`${url}`);
      setCategories(response.data);
      setLoading(false);
    } catch (err) {
      console.error(err);
    }
  };

  const handleCategorie = (cat_id, index) => {
    setCatType(cat_id);
    setIsActive(index);
    setIsActiveAll(false);
    setFilteredProducts(
      products.filter((product) => product.category_id._id === cat_id)
    );
  };

  // Fetch Products
  const getProducts = async () => {
    try {
      const response = await axios.get(`${urlProduct}`);
      setProducts(response.data);
      setFilteredProducts(response.data);
      setLoading(false);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getCategories();
    getProducts();

    const interval = setInterval(() => {
      getCategories();
      getProducts();
    }, 2 * 60 * 60 * 1000); // 2 hours in milliseconds
    
    // Clean up the interval when the component unmounts
    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <>
      <div className="prodpage-header">
        <h1>Our Plants</h1>
      </div>
      {loading ? (
        <LoadingScreen />
      ) : (
        <div className="prodpage-row-container">
          <div className="prodpage-cat">
            <h2>Categories</h2>

            <ul>
              <Link
                style={{ color: isActiveAll === true ? "green" : "" }}
                onClick={handleAllProducts}
              >
                All Products
              </Link>
              {categories.map((cat, index) => {
                return (
                  <Link
                    style={{
                      color: isActive === index ? "green" : "",
                    }}
                    onClick={() => handleCategorie(cat._id, index)}
                  >
                    {cat.name}
                  </Link>
                );
              })}
            </ul>
          </div>

          <div className="prodpage-products">
            {/* <h2>Evergreen</h2> */}
            <div className="prodpage-productCartContainer">
              {filteredProducts.map((product) => {
                return (
                  <ProductCard
                    id={product._id}
                    img={product.file[0]}
                    name={product.name}
                    price={product.price}
                    points={product.points}
                    desc={product.desc.substring(0, 25)}
                  />
                );
              })}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ProductsPage;
