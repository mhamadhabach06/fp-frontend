import axios from "axios";
import React, { useState } from "react";
import "./ContactUs.css";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ContactUs = () => {
  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [msg, setMsg] = useState();

  const [emailSent, setEmailSent] = useState(false);
  const contactURL = process.env.REACT_APP_URL+"/contactus"

  const notify = () => toast.success("Thank you.Your message has been received");

  const submit = () => {
    sendMessage();
    if (name && email && msg) {
      setName("");
      setEmail("");
      setMsg("");
      setEmailSent(true);
    }
  };

  const sendMessage = async () => {
    if(name.length != 0 || email.length !=0 || msg.length!=0)
    try {
      const res = await axios.post(`${contactURL}`, {
        name: name,
        message: msg,
        email:email
      });
      notify();

    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="top-container-contact">
      <div class="container-contact">
        <div class="content">
          <div class="left-side">
            <div class="address details">
              <i class="fas fa-map-marker-alt"></i>
              <div class="topic">Address</div>
              <div class="text-one">Lebanon, Beirut</div>
              <div class="text-two">Hamra Street</div>
            </div>
            <div class="phone details">
              <i class="fas fa-phone-alt"></i>
              <div class="topic">Phone</div>
              <div class="text-one">+961 76666666</div>
              <div class="text-two">+961 71111111</div>
            </div>
            <div class="email details">
              <i class="fas fa-envelope"></i>
              <div class="topic">Email</div>
              <div class="text-one">plantify@gmail.com</div>
              <div class="text-two">info.plantify@gmail.com</div>
            </div>
          </div>
          <div class="right-side">
            <div class="topic-text">Send us a message</div>
            <br />
            <form>
              <div class="input-box">
                <input 
                type="text" 
                placeholder="Enter your name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
             />
              </div>
              <div class="input-box">
                <input type="text" 
                placeholder="Enter your email"
                value={email}
                required
                onChange={(e) => setEmail(e.target.value)} 
                />
              </div>
              <div class="input-box message-box">
                <textarea
                  placeholder="Enter Your Message"
                  cols="30"
                  rows="10"
                  required
                  value={msg}
                onChange={(e) => setMsg(e.target.value)}
                ></textarea>
              </div>
              <div class="button">
                <input type="button" onClick={submit} value="Send Message" />
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="space" style={{marginBottom:"52vh"}}>
        
      </div>
    </div>
  );
};

export default ContactUs;
