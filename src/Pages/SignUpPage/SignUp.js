import React, { useEffect, useState } from "react";
import "./SignUp.css";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import { toast } from "react-toastify";
const SignUp = () => {
  const [emailLogin, setEmailLogin] = useState("");
  const [passLogin, setPassLogin] = useState("");

  const [name, setName] = useState("");
  const [emailRegister, setEmailRegister] = useState("");
  const [passRegister, setPassRegister] = useState("");

  const urlRegister = process.env.REACT_APP_URL + "/users";
  const urlLogin = process.env.REACT_APP_URL + "/users/login";

  let token = localStorage.getItem("token");
  let navigate = useNavigate();

  const notifyRegister = () => toast.success("Successfully Registered");
  const notifyLogin = () => toast.success("Logged In Successfully");

  useEffect(() => {
    const checkIfLoggedIn = async () => {
      let token = localStorage.getItem("token");

      if (token) navigate("/", { replace: true });
    };
    checkIfLoggedIn();
  }, []);

  const registerFunc = async () => {
    try {
      const response = await axios.post(`${urlRegister}`, {
        name: name,
        email: emailRegister,
        password: passRegister,
      });
      const res = response.data;
      localStorage.setItem("token", res.token);
      navigate("/");
      notifyRegister();
      // if (name && emailRegister && passRegister) {
      //   setName("");
      //   setEmailRegister("");
      //   setPassRegister("");
      // }
    } catch (error) {
      console.log(error);
    }
  };

  const loginFunc = async () => {
    try {
      const response = await axios.post(`${urlLogin}`, {
        email: emailLogin,
        password: passLogin,
      });
      const res = response.data;
      localStorage.setItem("token", res.token);
      navigate("/");
      notifyLogin();
    } catch (error) {
      console.log(error);
    }
  };

  const registerDesign = () => {
    document.getElementById("login").style.left = "-400px";
    document.getElementById("register").style.left = "50px";
    document.getElementsById("btn1").style.left = "110px";
  };

  const loginDesign = () => {
    document.getElementById("login").style.left = "50px";
    document.getElementById("register").style.left = "450px";
    document.getElementsById("btn1").style.left = "0px";
  };
  return (
    <div className="SignUp-hero">
      <div className="form-box">
        <div className="btn-box">
          <div id="btn1"></div>
          <button
            type="button"
            className="toogle-btnn login-SignUP"
            onClick={loginDesign}
          >
            Log In
          </button>
          <button
            type="button"
            className="toogle-btnn"
            onClick={registerDesign}
          >
            Register
          </button>
        </div>
        <div className="social-icons-s">
          <i className="fa-brands fa-facebook-f uu"></i>
          <i className="fa-brands fa-instagram uu"></i>
          <i className="fa-brands fa-twitter uu"></i>
        </div>

        <form id="login" className="input-group">
          <input
            type="email"
            className="input-field-s"
            placeholder="Email"
            value={emailLogin}
            onChange={(e) => setEmailLogin(e.target.value)}
            required
          />
          <input
            type="password"
            className="input-field-s"
            placeholder="Password"
            value={passLogin}
            onChange={(e) => setPassLogin(e.target.value)}
            required
          />
          <Link onClick={loginFunc} className="submit-btn">
            Login
          </Link>
        </form>

        <form id="register" className="input-group">
          <input
            type="text"
            className="input-field-s"
            placeholder="Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />

          <input
            type="email"
            className="input-field-s"
            placeholder="Email"
            value={emailRegister}
            onChange={(e) => setEmailRegister(e.target.value)}
            required
          />
          <input
            type="password"
            className="input-field-s"
            placeholder="Password"
            value={passRegister}
            onChange={(e) => setPassRegister(e.target.value)}
            required
          />
          <Link onClick={registerFunc} className="submit-btn">
            Register
          </Link>
        </form>
      </div>
    </div>
  );
};

export default SignUp;
