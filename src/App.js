import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Homepage from "./Pages/HomePage/HomePage";
import ProductsPage from "./Pages/Products/ProductsPage";
import NavBar from "./Components/Navbar/NavBar";
import ContactUs from "./Pages/ContactUsPage/ContactUs";
import { ToastContainer } from "react-toastify";
import Footer from "./Components/Footer/Footer";
import SignUp from "./Pages/SignUpPage/SignUp";
import ProductDetail from "./Pages/ProductDetail/ProductDetail";
import Cart from "./Components/Cart/Cart";
import Checkout from "./Pages/CheckoutPage/Checkout";

function App() {
  return (
    <div className="App">
      <Router>
      <Cart/>
        <Routes>
          <Route exact path="/" element={<Homepage />} />
          <Route
            exact
            path="/products"
            element={
              <>
                <NavBar /> <ProductsPage />
              </>
            }
          />
          <Route
            exact
            path="/contact"
            element={
              <>
                <NavBar /> <ContactUs />
                <Footer />
              </>
            }
          />
          <Route
            exact
            path="/signup"
            element={
              <>
                <NavBar />
                <SignUp />
              </>
            }
          />
          <Route
            exact
            path="/productDetail"
            element={
              <>
                <NavBar />
                <ProductDetail />
                <Footer />
              </>
            }
          />
          <Route
            exact
            path="/checkout"
            element={
              <>
                <NavBar />
                <Checkout />
                <Footer />
              </>
            }
          />
        </Routes>
      </Router>
      <ToastContainer />
    </div>
  );
}

export default App;
